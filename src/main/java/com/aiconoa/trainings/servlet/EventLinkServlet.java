package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.Resource;
import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.Hasher;
import com.aiconoa.trainings.services.LoggerPrinter;
import com.aiconoa.trainings.services.SendMailBySite;

/**
 * Servlet implementation class eventLinkServlet
 */
@WebServlet("/EventLinkServlet")
public class EventLinkServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;
    @Resource(name = "java:jboss/mail/Gmail")
    private Session session;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!EventService.checkIfExistUniqueById("events", idEvent, eventsdb)) {
                LOGGER.fatal("Le paramètre idEvent entré n'existe pas dans la bdd - dans le doGet de EventStatServlet");
                LoggerPrinter.print404(response);
                return;
            }

            // recherche de l'evenement par l'idEvent
            Event event = EventService.selectEventColumnsByIdEvent(eventsdb, idEvent);
            // recherche des membres ajoutés et statuts pour l'IdEvent
            List<Subscriber> subscribers = EventService.listSelectSubscribersByIdEvent(eventsdb, idEvent);

            // envoi des arg a la jsp
            request.setAttribute("event", event);
            request.setAttribute("subscriberList", subscribers);
            request.getRequestDispatcher("/WEB-INF/eventLink.jsp").forward(request, response);

        } catch (EventServiceException eventServiceException) {
            LOGGER.fatal("probleme avec la requete sql dans le doGet de EventLinkServlet", eventServiceException);
            LoggerPrinter.print500(response, eventServiceException);
            return;
        } catch (Exception e) {
            LoggerPrinter.print500(response, e);
            LOGGER.fatal("probleme avec le forward du dispatcher", e);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idUser = 0;
        String hashcodeString = null;
        try {
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!EventService.checkIfExistUniqueById("events", idEvent, eventsdb)) {
                LOGGER.fatal("Le paramètre idEvent entré n'existe pas dans la bdd - dans le doGet de EventStatServlet");
                LoggerPrinter.print404(response);
                return;
            }
            // recherche de l'evenement par l'idEvent
            Event event = EventService.selectEventColumnsByIdEvent(eventsdb, idEvent);

            String emailList = request.getParameter("email");
            if (emailList == null || emailList.trim().isEmpty()) {
                response.sendRedirect("EventLinkServlet?id=" + idEvent);
                return;
            }
            String[] splits = emailList.split(";");

            for (String email : splits) {
                // prochainement: a tester si email est sous la forme d'un email

                idUser = EventService.insertUserIfDontExist(eventsdb, email);
                if (idUser == 0) {
                    // Si pas de membre crée=> membre existant=> on cherche son
                    // id
                    User user = EventService.selectUserIdByMail(eventsdb, email);
                    idUser = user.getIdUser();
                }
                // On cherche si le duo (idEvent/idUser) existe déjà dans la
                // table subscribers
                boolean isSubscribersDuoIdEventIdUserIsUnique = EventService.isSubscribersDuoIdEventIdUserIsUnique(eventsdb, idEvent, idUser);
                if (!isSubscribersDuoIdEventIdUserIsUnique) {
                    // pas d'occurance =>création
                    hashcodeString = Hasher.bytesToHex(Hasher.hash("SHA-1", email + "#" + idEvent));
                    EventService.insertSubscriber(eventsdb, idEvent, idUser, hashcodeString);
                } else { // couple deja existant => on cherche le hashcode
                    hashcodeString = EventService.hashcodeByIdEventIdUser(eventsdb, idEvent, idUser);
                }
                SendMailBySite.sendMail(session, email, event.getTitle(), request, hashcodeString);
            }
            response.sendRedirect("EventLinkServlet?id=" + idEvent);

        } catch (NumberFormatException e) {
            LOGGER.fatal("Le parametre de la requete http ne peut pas etre converti en Integer dans le doPost de EventLinkServlet", e);
            LoggerPrinter.print404(response, e);
        } catch (EventServiceException eventServiceException) {
            LoggerPrinter.print500(response, eventServiceException);
            return;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.fatal("probleme avec la requete sql dans le doPost de EventLinkServlet", e);
            LoggerPrinter.print500(response, e);
            return;
        }catch (Exception exception) {
            LoggerPrinter.print500(response, exception);
            return;
        }
    }
}
