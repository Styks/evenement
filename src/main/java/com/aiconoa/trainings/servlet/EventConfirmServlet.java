package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HashCodeChecker;
import com.aiconoa.trainings.services.LoggerPrinter;

@WebServlet("/EventConfirmServlet")
public class EventConfirmServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;

    private static final int ACCEPT = 1;
    private static final int DECLINE = 2;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String hashcodeString = request.getParameter("token");
            if (!HashCodeChecker.checkHashCode(hashcodeString, eventsdb, response)) {
                return;
            }

            Subscriber subscriber = EventService.selectSubscriberIdEventIdUserByHashcode(eventsdb, hashcodeString);
            Event event = EventService.selectEventColumnsByIdEvent(eventsdb, subscriber.getIdEvent());
            User user = EventService.selectUserEmailByIdUser(eventsdb, subscriber.getIdUser());

            request.setAttribute("event", event);
            request.setAttribute("email", user.getEmail());

            request.getRequestDispatcher("/WEB-INF/eventConfirm.jsp").forward(request, response);
        } catch (EventServiceException e) {
            LoggerPrinter.print500(response, e);
            return;
        } catch (Exception e) {
            LOGGER.fatal("Probleme avec le forward du dispatcher dans le doGet de EventConfirmServlet", e);
            LoggerPrinter.print500(response, e);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String hashcodeString = request.getParameter("token");
            if (!HashCodeChecker.checkHashCode(hashcodeString, eventsdb, response)) {
                return;
            }

            int responseUser;
            switch (request.getParameter("responseUser")) {
            case "Je viens!":
                responseUser = ACCEPT;
                break;
            case "Je viens pas":
                responseUser = DECLINE;
                break;
            default:
                LOGGER.fatal("probleme avec la requete http coté client dans le doPost de EventConfirmServlet");
                LoggerPrinter.print404(response);
                return;
            }

            EventService.updateSubscribersIsPresentWhereHashCode(hashcodeString, responseUser, eventsdb);
            redirection(hashcodeString, response);

        } catch (EventServiceException eventServiceException) {
            LoggerPrinter.print500(response, eventServiceException);
            return;
        } catch (Exception exception) {
            LoggerPrinter.print404(response, exception);
            return;
        }
    }

    private void redirection(String hashcodeString, HttpServletResponse response)
            throws SQLException, IOException {

        response.sendRedirect("EventConfirmValidationServlet?token=" + hashcodeString);
    }

}
