package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.LoggerPrinter;

@WebServlet("/EventCreationServlet")
public class EventCreationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            if (request.getParameter("id") != null) {
                int idEvent = Integer.parseInt(request.getParameter("id"));
                if (!EventService.checkIfExistUniqueById("events", idEvent, eventsdb)) {
                    LOGGER.fatal("Le paramètre idEvent entré n'existe pas dans la bdd - dans le doGet de EventStatServlet");
                    LoggerPrinter.print404(response);
                    return;
                }
                Event event = EventService.selectEventColumnsByIdEvent(eventsdb, idEvent);
                request.setAttribute("event", event);
            }
            request.getRequestDispatcher("/WEB-INF/eventCreation.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            LOGGER.fatal("Le parametre de la requete http ne peut pas etre converti en Integer dans le doGet de EventCreationServlet", e);
            LoggerPrinter.print404(response, e);
            return;
        } catch (EventServiceException eventServiceException) {
            LoggerPrinter.print500(response, eventServiceException);
            return;
        } catch (IOException e) {
            LOGGER.fatal("probleme avec le forward du dispatcher dans le doGet de EventCreationServlet", e);
            LoggerPrinter.print500(response, e);
            return;
        } catch (Exception exception) {
            LoggerPrinter.print404(response, exception);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String idEventString = request.getParameter("id");
        int idEvent;
        try {
            if (idEventString != null) {
                idEvent = Integer.parseInt(idEventString);
                if (!EventService.checkIfExistUniqueById("events", idEvent, eventsdb)) {
                    LOGGER.fatal("Le paramètre idEvent entré n'existe pas dans la bdd - dans le doGet de EventStatServlet");
                    LoggerPrinter.print404(response);
                    return;
                }
                EventService.updateEvent(eventsdb, idEvent, title, description);
            } else {
                idEvent = EventService.insertEvent(eventsdb, title, description);
            }
            response.sendRedirect("EventLinkServlet?id=" + idEvent);
        } catch (NumberFormatException e) {
            LOGGER.fatal("Le parametre de la requete http ne peut pas etre converti en Integer dans le doPost de EventCreationServlet", e);
            LoggerPrinter.print404(response, e);
            return;
        } catch (IOException e) {
            LOGGER.fatal("probleme avec la redirection dans le doPost de EventCreationServlet", e);
        } catch (EventServiceException eventServiceException) {
            LoggerPrinter.print500(response, eventServiceException);
        } catch (Exception exception) {
            LoggerPrinter.print404(response, exception);
            return;
        }
    }
}
