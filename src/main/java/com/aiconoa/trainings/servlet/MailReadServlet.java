package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.LoggerPrinter;

/**
 * Servlet implementation class MailReadServlet
 */
@WebServlet("/MailReadServlet")
public class MailReadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;
    private static final int MAILREAD = 1;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String hashcodeString = request.getParameter("msg_id");
        try {
            EventService.updateSubscribersMailStatusWhereHashcode(MAILREAD, hashcodeString, eventsdb);
            request.getRequestDispatcher("/images/icone_valider.png")
                    .forward(request, response);
        } catch (EventServiceException e) {
            LoggerPrinter.print500(response, e);
            return;
        } catch (Exception e) {
            LOGGER.fatal("probleme avec le forward du dispatcher", e);
            return;
        }
    }
}
