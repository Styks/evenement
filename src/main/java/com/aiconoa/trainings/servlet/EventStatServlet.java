package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.EventStatData;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.LoggerPrinter;

/**
 * Servlet implementation class EventStatServlet
 */
@WebServlet("/EventStatServlet")
public class EventStatServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;
    private static final int ACCEPT = 1;
    private static final int DECLINE = 2;
    private static final int PRESENT = 1;
    private static final int NOTPRESENT = 2;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!EventService.checkIfExistUniqueById("events", idEvent, eventsdb)) {
                LOGGER.fatal("Le paramètre idEvent entré n'existe pas dans la bdd - dans le doGet de EventStatServlet");
                LoggerPrinter.print404(response);
                return;
            }
            EventStatData eventStatData = new EventStatData();
            eventStatData.setIdEvent(idEvent);

            // recherche de l'evenement
            Event event = EventService.selectEventColumnsByIdEvent(eventsdb, idEvent);

            // recherche des membres ajoutés et statuts
            List<Subscriber> subscriberList = EventService.listSelectSubscribersByIdEvent(eventsdb, idEvent);
            eventStatData.setNbTotalInvitation(subscriberList.size());

            // recherche du nombre de participants ACCEPT
            eventStatData.setNbIsPresent(EventService.countNumberPresentOrNot(idEvent, ACCEPT, eventsdb));

            // recherche du nombre de participants DECLINE
            eventStatData.setNbIsNotPresent(EventService.countNumberPresentOrNot(idEvent, DECLINE, eventsdb));

            // recherche du nombre de mails lus
            eventStatData.setNbIsRead(EventService.countNumberMailRead(idEvent, eventsdb));

            // recherche du nombre de presents apres ouverture du mail
            eventStatData.setNbIsPresentAfterReading(
                    EventService.countNbIsPresentOrNotAfterReading(idEvent, PRESENT, eventsdb));

            // recherche du nombre d'absents apres ouverture du mail
            eventStatData.setNbIsAbsentAfterReading(
                    EventService.countNbIsPresentOrNotAfterReading(idEvent, NOTPRESENT, eventsdb));

            // passage des arguments a la jsp
            request.setAttribute("subscriberList", subscriberList);
            request.setAttribute("eventStatData", eventStatData);
            request.setAttribute("title", event.getTitle());

            request.getRequestDispatcher("/WEB-INF/eventStat.jsp").forward(request, response);

        } catch (EventServiceException e) {
            LoggerPrinter.print500(response, e);
            return;
        } catch (NumberFormatException e) {
            LOGGER.info("Le parametre de la requete http ne peut pas etre converti en Integer dans le doGet de EventStatServlet", e);
            LoggerPrinter.print404(response, e);
            return;
        } catch (Exception exception) {
            LoggerPrinter.print404(response, exception);
            return;
        }
    }
}
