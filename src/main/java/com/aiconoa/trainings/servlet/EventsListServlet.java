package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.LoggerPrinter;

@WebServlet("/EventsListServlet")
public class EventsListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Resource(lookup = "java:jboss/DataSources/eventsdb")
    private DataSource eventsdb;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Event> events =EventService.selectAllEventColumns(eventsdb);
            request.setAttribute("events", events);
            request.getRequestDispatcher("/WEB-INF/eventsList.jsp").forward(request, response);
        } catch (EventServiceException e) {
            LoggerPrinter.print500(response, e);
            return;
        } catch (Exception e) {
            LOGGER.info("probleme avec le forward du dispatcher", e);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.sendRedirect("EventCreationServlet");
        } catch (Exception e) {
            LOGGER.info("probleme avec la redirection dans le doPost de EventsListServlet", e);
        }
    }

}
