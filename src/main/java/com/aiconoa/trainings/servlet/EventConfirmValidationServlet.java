package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.LoggerPrinter;

@WebServlet("/EventConfirmValidationServlet")
public class EventConfirmValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger();
	@Resource(lookup = "java:jboss/DataSources/eventsdb")
	private DataSource eventsdb;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		try {
            String hashcodeString = request.getParameter("token");
            
            Subscriber subscriber = EventService.selectSubscriberIdEventIdUserIsPresentByHashcode(eventsdb, hashcodeString);
		    Event event = EventService.selectEventColumnsByIdEvent(eventsdb, subscriber.getIdEvent());
            User user = EventService.selectUserEmailByIdUser(eventsdb, subscriber.getIdUser());

			request.setAttribute("event", event);
			request.setAttribute("email", user.getEmail());
			request.setAttribute("isPresent", subscriber.getIsPresentString());	
			request.getRequestDispatcher("/WEB-INF/eventConfirmValidation.jsp")
				   .forward(request, response);
			
		} catch (NumberFormatException e) {
			LOGGER.fatal("Le parametre de la requete http ne peut pas etre converti en Integer dans le doGet de EventConfirmValidationServlet", e);
			LoggerPrinter.print404(response, e);
			return;
		} catch (EventServiceException eventServiceException) {
			LOGGER.fatal("probleme avec la requete sql dans le doGet de EventConfirmValidationServlet", eventServiceException);
			LoggerPrinter.print500(response, eventServiceException);
			return;
		} catch (Exception e) {
			LOGGER.fatal("Probleme avec le forward du dispatcher dans le doGet de EventConfirmValidationServlet", e);
			LoggerPrinter.print500(response, e);
			return;
		}
	}

}