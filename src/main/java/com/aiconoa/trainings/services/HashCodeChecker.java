package com.aiconoa.trainings.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public class HashCodeChecker {

    private HashCodeChecker() {
        super();
    }

    public static boolean checkHashCode(String hashcodeString, DataSource eventsdb, HttpServletResponse response) {
        if (hashcodeString == "") {
            LoggerPrinter.print404(response);
            return false;
        }
        try (Connection connectionDB = eventsdb.getConnection()) {
            String sql = "SELECT COUNT(*) from subscribers WHERE hashcode=?";
            int countNbHashcode = 0;
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, hashcodeString);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    countNbHashcode = resultSet.getInt("COUNT(*)");
                }
            }
            if (countNbHashcode != 1) {
                LoggerPrinter.print404(response);
                return false;
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with count subscribers for hashcode=" + hashcodeString, e);
        }
        return true;
    }
}