package com.aiconoa.trainings.services;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerPrinter {

    private static final Logger LOGGER = LogManager.getLogger();

    private LoggerPrinter() {
        super();
    }

    public static void print404(HttpServletResponse response, Exception e) {
        LOGGER.fatal(e);
        try {
            response.sendError(404, "page introuvable");
        } catch (Exception e1) {
            LOGGER.fatal(e1);
        }
    }

    public static void print404(HttpServletResponse response) {
        LOGGER.fatal("Page non atteinte");
        try {
            response.sendError(404, "page introuvable");
        } catch (Exception e1) {
            LOGGER.fatal(e1);
        }
    }

    public static void print500(HttpServletResponse response, SQLException e) {
        LOGGER.fatal("probleme avec la requete sql", e);
        try {
            response.sendError(500, "Something wrong append");
        } catch (Exception e1) {
            LOGGER.fatal(e1);
        }
    }

    public static void print500(HttpServletResponse response, Exception e) {
        LOGGER.fatal("probleme server", e);
        try {
            response.sendError(500, "Something wrong append");
        } catch (Exception e1) {
            LOGGER.fatal(e1);
        }
    }
}
