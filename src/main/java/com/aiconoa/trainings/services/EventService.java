package com.aiconoa.trainings.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;

public class EventService {
    private static final String EMAIL = "email";
    private static final String ISPRESENT = "isPresent";

    private EventService() {
        super();
    }

    public static Event selectEventColumnsByIdEvent(DataSource eventsdb, int idEvent) {
        String title = null;
        String description = null;
        String sql = "SELECT title, description FROM events WHERE id=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    title = resultSet.getString("title");
                    description = resultSet.getString("description");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select event colums by idEvent for idEvent" + idEvent, e);
        }
        return new Event(title, description, idEvent);
    }

    public static List<Event> selectAllEventColumns(DataSource eventsdb) {
        String sql = "SELECT * FROM events";
        List<Event> events = new ArrayList<>();
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    events.add(new Event(resultSet.getString("title"), resultSet.getString("description"), resultSet.getInt("id")));
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select all events", e);
        }
        return events;
    }

    public static Subscriber selectSubscriberIdEventIdUserByHashcode(DataSource eventsdb, String hashcodeString) {
        int idEvent = 0;
        int idUser = 0;
        String sql = "SELECT idEvent, idUser FROM subscribers WHERE hashCode=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, hashcodeString);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    idEvent = resultSet.getInt("idEvent");
                    idUser = resultSet.getInt("idUser");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select subscriber by hashcode for hashcode " + hashcodeString, e);
        }
        return new Subscriber(idEvent, idUser);
    }

    public static Subscriber selectSubscriberIdEventIdUserIsPresentByHashcode(DataSource eventsdb, String hashcodeString) {
        int idEvent = 0;
        int idUser = 0;
        int isPresent = 0;
        String sql = "SELECT idEvent, idUser, isPresent FROM subscribers WHERE hashCode=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, hashcodeString);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    idEvent = resultSet.getInt("idEvent");
                    idUser = resultSet.getInt("idUser");
                    isPresent = resultSet.getInt(ISPRESENT);
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select subscriber by hashcode for hashcode " + hashcodeString, e);
        }
        return new Subscriber(idEvent, idUser, isPresent);
    }

    public static User selectUserEmailByIdUser(DataSource eventsdb, int idUser) {
        String email = null;
        String sql = "SELECT email FROM users WHERE id=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idUser);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    email = resultSet.getString(EMAIL);
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select User by idUser for idUser=" + idUser, e);
        }
        return new User(idUser, email);
    }

    public static List<Subscriber> listSelectSubscribersByIdEvent(DataSource eventsdb, int idEvent) {
        List<Subscriber> subscribers = new ArrayList<>();
        String sql = "SELECT users.email,subscribers.isPresent, subscribers.sendingDate FROM users INNER JOIN subscribers ON users.id = subscribers.iduser WHERE (subscribers.idEvent=?)";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    subscribers.add(new Subscriber(resultSet.getString(EMAIL), resultSet.getInt(ISPRESENT),
                            resultSet.getTimestamp("sendingDate")));
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select Subscriber by idEvent for idEvent=" + idEvent, e);
        }
        return subscribers;
    }

    public static User selectUserIdByMail(DataSource eventsdb, String email) {
        String sql = "SELECT id FROM users WHERE email=?";
        int idUser = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, email);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    idUser = resultSet.getInt("id");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Select User by Mail for mail =" + email, e);
        }
        return new User(idUser, email);
    }

    public static boolean checkIfExistUniqueById(String table, int id, DataSource eventsdb) {
        String sql = "SELECT COUNT(*) AS nb FROM " + table + " WHERE id=?";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nb");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with count number sql in table =" + table, e);
        }
        return nb == 1;
    }

    public static boolean checkIfExistUniqueByEmail(String table, String email, DataSource eventsdb) {
        String sql = "SELECT COUNT(*) as nb FROM " + table + " WHERE email=?";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, email);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nb");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with count number sql in table =" + table, e);
        }
        return nb == 1;
    }

    public static int countNumberPresentOrNot(int idEvent, int status, DataSource eventsdb) {
        String sql = "SELECT COUNT(*) as nb FROM subscribers WHERE idEvent=? AND isPresent=?";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                preparedStatement.setInt(2, status);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nb");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with count number present for idEvent=" + idEvent, e);
        }
        return nb;
    }

    public static int countNumberMailRead(int idEvent, DataSource eventsdb) {
        String sql = "SELECT COUNT(*) as nbOpen FROM subscribers WHERE idEvent=? AND mailStatus=1";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nbOpen");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with return number mail for idEvent=" + idEvent, e);
        }
        return nb;
    }

    public static int countNbIsPresentOrNotAfterReading(int idEvent, int isPresent, DataSource eventsdb) {
        String sql = "SELECT COUNT(*) as nb FROM subscribers WHERE idEvent=? AND mailStatus=1 AND isPresent=?";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                preparedStatement.setInt(2, isPresent);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nb");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with return number present after reading mail for idEvent=" + idEvent, e);
        }
        return nb;
    }

    public static boolean isSubscribersDuoIdEventIdUserIsUnique(DataSource eventsdb, int idEvent, int idUser) {
        String sql = "SELECT COUNT(*) as nb from subscribers WHERE idEvent=? AND idUser=?";
        int nb = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                preparedStatement.setInt(2, idUser);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    nb = resultSet.getInt("nb");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with isSubscribersDuoIdEventIdUserIsUnique for idEvent=" + idEvent + " abd idUser = " + idUser, e);
        }
        return nb == 1;
    }

    public static String hashcodeByIdEventIdUser(DataSource eventsdb, int idEvent, int idUser) {
        String sql = "SELECT hashCode FROM subscribers WHERE idEvent=? AND idUser=?";
        String hashcodeString = null;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                preparedStatement.setInt(2, idUser);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    hashcodeString = resultSet.getString("hashCode");
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with select hashcode for idEvent=" + idEvent + " abd idUser = " + idUser, e);
        }
        return hashcodeString;
    }

    public static void updateEvent(DataSource eventsdb, int idEvent, String title, String description) {
        String sql = "UPDATE events SET title = ? , description = ? WHERE id = ?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setString(1, title);
                preparedStatement.setString(2, description);
                preparedStatement.setInt(3, idEvent);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with updateEvent for idEvent=" + idEvent, e);
        }
    }

    public static int insertEvent(DataSource eventsdb, String title, String description) {
        String sql = "INSERT INTO events (title, description) VALUES (?,?)";
        int idEvent = 0;
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, title);
                preparedStatement.setString(2, description);
                preparedStatement.executeUpdate();
                ResultSet rs = preparedStatement.getGeneratedKeys();
                if (rs.next()) {
                    idEvent = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with insert Event", e);
        }
        return idEvent;
    }

    public static void updateSubscribersIsPresentWhereHashCode(String hashcodeString, int responseUser, DataSource eventsdb) {
        String sql = "UPDATE subscribers SET isPresent = ? WHERE hashCode = ?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, responseUser);
                preparedStatement.setString(2, hashcodeString);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Update subscribers presence for hashCode" + hashcodeString, e);
        }
    }

    public static int insertUserIfDontExist(DataSource eventsdb, String email) {
        int idUser = 0;
        String sql = "INSERT INTO users (email) VALUES (?) ON DUPLICATE KEY UPDATE email=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, email);
                preparedStatement.setString(2, email);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    idUser = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with insertUserIfDontExist for email" + email, e);
        }
        return idUser;
    }

    public static void updateSubscribersMailStatusWhereHashcode(int mailread, String hashcodeString, DataSource eventsdb) {
        String sql = "UPDATE subscribers set mailStatus=? Where hashCode=?";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, mailread);
                preparedStatement.setString(2, hashcodeString);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with Update subscribers presence for hashCode" + hashcodeString, e);
        }
    }

    public static void insertSubscriber(DataSource eventsdb, int idEvent, int idUser, String hashcodeString) {
        String sql = "INSERT INTO subscribers (idEvent,idUser,hashCode) VALUES (?,?,?)";
        try (Connection connectionDB = eventsdb.getConnection()) {
            try (PreparedStatement preparedStatement = connectionDB.prepareStatement(sql)) {
                preparedStatement.setInt(1, idEvent);
                preparedStatement.setInt(2, idUser);
                preparedStatement.setString(3, hashcodeString);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new EventServiceException("Error with insert subscribers", e);
        }
    }
}