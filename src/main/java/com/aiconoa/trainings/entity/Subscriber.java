package com.aiconoa.trainings.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Subscriber {
	private int idEvent;
	private int idUser;
	private String email;
	private int isPresent;
	private String isPresentString;
	private String sendingDateString;
	private String iconeLink;
	private static final int ACCEPT = 1;
	private static final int DECLINE = 2;

    public Subscriber(int idEvent, int idUser) {
        super();
        this.idEvent = idEvent;
        this.idUser = idUser;
    }
    
    public Subscriber(int idEvent, int idUser, int isPresent) {
        super();
        this.idEvent = idEvent;
        this.idUser = idUser;
        this.isPresent = isPresent;
        
        checkIsPresent(isPresent);
    }
    
    public Subscriber(String email, int isPresent, Date sendingDate) {
		super();
		this.email = email;
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
		this.sendingDateString = formater.format(sendingDate);
		this.isPresent = isPresent;

        checkIsPresent(isPresent);
	}

	public String getIconeLink() {
		return iconeLink;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getSendingDateString() {
		return sendingDateString;
	}

	public int getIsPresent() {
		return isPresent;
	}

	public void setIsPresent(int isPresent) {
		this.isPresent = isPresent;
	}

	public String getIsPresentString() {
		return isPresentString;
	}

	public int getIdEvent() {
		return idEvent;
	}

	public int getIdUser() {
		return idUser;
	}

	private void checkIsPresent(int isPresent) {
	    switch (isPresent) {
	    case ACCEPT:this.isPresentString = "Présent";
	    this.iconeLink = "images/icone_valider.png";
	    break;
	    case DECLINE:this.isPresentString = "Absent";
	    this.iconeLink = "images/icon_delete.png";
	    break;
	    default:
	        this.isPresentString = "Non confirmé";
	        this.iconeLink = "images/icone_interrogation.png";
	        return;
	    }
	}
}

