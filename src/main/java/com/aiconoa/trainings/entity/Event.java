package com.aiconoa.trainings.entity;

public class Event {
    private String title;
    private String description;
    private String link;
    private int idEvent;

    public Event(String title, String description, int idEvent) {
        super();
        this.title = title;
        this.description = description;
        this.link = "EventLinkServlet?id=" + idEvent;
        this.setIdEvent(idEvent);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

}
