<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Creation d'evenements</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<c:choose>
		<c:when test="${empty event.idEvent}">
			<div class="wrapper">
				<h1>Creation d'évènements</h1>
				<p>Inserer un titre et une descprition svp</p>
				<form class="form" method="post">
					<input id="title" type="text" name="title"
						placeholder="Entrer un titre" />
					<textarea rows="4" name="description"
						placeholder="Entrer une description..."></textarea>
					<input type="submit" class="submit" value="Créer!">
				</form>
			</div>
			<p class="optimize">Optimized for Javengers!</p>
		</c:when>
		<c:otherwise>
			<nav>
				<ul class="menu">
					<li><a href="EventCreationServlet?id=${event.idEvent}">Evenement</a></li>
					<li><a href="EventLinkServlet?id=${event.idEvent}">Statut</a></li>
					<li><a href="EventStatServlet?id=${event.idEvent}">Statistiques</a></li>
				</ul>
			</nav>
			<br>
			<br>
			<div class="wrapper">
				<h1>Edition d'évènements</h1>
				<p>Inserer un titre et une descprition svp</p>
				<form class="form" method="post">
					<input id="title" type="text" name="title" value="${event.title}" />
					<textarea rows="4" name="description">${event.description}</textarea>
					<input type="submit" class="submit" value="Editer!">
				</form>
			</div>
			<p class="optimize">Optimized for Javengers!</p>
		</c:otherwise>
	</c:choose>
</body>
</html>