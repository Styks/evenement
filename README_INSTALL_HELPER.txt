- download jdk 1.8.0_111 (java development kit)
	cr�er une variable d'environnement JAVA_HOME pour le dossier bin du jdk
	ajouter la variable au Path

- download apache-maven-3.3.9-bin
	cr�er une variable d'environnement MAVEN_HOME pour le dossier bin de maven
	ajouter la variable au Path

- download wildfly 10.0.0.Final (serveur d'applications)
	cr�er une variable d'environnement JBOSS_HOME pour le dossier bin de wildfly
	   dans le dossier bin
	   executer : add-user.bat (pour windows)
		      add-user.sh (pour linux)
	   ne mets pas un mot de passe trop compliqu� a te rappeler !! :)
	   executer : standalone.bat (pour windows)
		      standalone.sh (pour linux)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  creation d'une base de donn�es
- download easyPHP (BDD)
	demarrer easyPHP
	ouvrir la console d'administration (http://127.0.0.1/home/)
	   ouvrir le module Administration MySQL : PhpMyAdmin 4.1.4
	      cr�er une nouvelle base de donn�e (on l'a appel�e eventsdb)
	importer le fichier .sql du dossier jeremy-pascal-evenement/src/dataBase
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

************************************************************************************************* 
  configuration de wildfly pour le connecter a la base de donn�es:
- download mysql-connector-java-5.1.4 (driver): https://dev.mysql.com/downloads/file/?id=465644
     dans la console d'administration de wildfly (localhost:9990):
	dans: Deployments : Add
		upload a new deployment
		chercher le mysql-connector-java-5.1.40-bin.jar
	dans: Configuration/Subsystems/DataSources/Non-XA : Add
		MySQL Datasource
		Name: eventsdb
		JNDI Name: java:jboss/DataSources/eventsdb (je crois que ce nom la n'a pas d'importance)
	   dans: Detected Driver choisir :
		mysql-connector-java-5.1.40-bin.jar_com.mysql.jdbc.Driver_5_1
*************************************************************************************************
	
-------------------------------------------------------------------------------------------------
  configuration de wildfly pour l'envoi de mails:
     dans la console d'administration de wildfly (localhost:9990):
	dans: Configuration/Socket Binding : View : view> OutBound Remote : Add
		Name: mail-smtp-gmail (ce nom est un exemple, il n'a pas d'importance)
		Host: smtp.gmail.com
		Port: 465 (587 devrait marcher aussi)
		Save
	dans: Configuration/Subsystems/Mail
		Name: java:jboss/mail/Gmail (je crois que ce nom la n'a pas d'importance)
		JNDI Name: java:jboss/mail/Gmail (nom de la ressource appelee dans la servlet)
		Save
		dans view :
			Socket Binding: mail-smtp-gmail (nom du Socket Binding defini plus haut)
			Type: smtp
			Username: adressemailpoubelle@gmail.com (la notre : pascaljeremym2i@gmail.com)
			Password: celui que tu a cr�� pour ton adresse mail poubelle (le notre: wildflym2i)
			COCHER : Use SSL
			Save
	redemarrer wildfly dans le shell ("Ctrl+C" puis "O" puis "standalone.bat(sh pour les linuxiens)")
	desactiver l'analyse du courrier sortant d'avast
-------------------------------------------------------------------------------------------------

dans le shell se placer dans le dossier ou se trouve le pom.xml (evenement)
taper la commande mvn clean wildfly:deploy

dans un navigateur, taper localhost:8080/http-jsp-servlet/adresse_definie_dans_une_Servlet_par_l'annotation_@WebServlet
		exemple : localhost:8080/http-jsp-servlet/EventCreationServlet